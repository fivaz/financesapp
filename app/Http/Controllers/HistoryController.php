<?php

namespace App\Http\Controllers;

use App\Account;
use App\Category;
use App\HistoryOfUpdates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($account_id)
    {
        $account = Account::find($account_id);

        return view('history.index')->withAccount($account);
    }

    static public function createTransference(Request $request)
    {
        $history = new HistoryOfUpdates(
            [
                'description' => $request->description,
                'value' => $request->value,
                'date' => $request->date,
                'status' => $request->status,
                'discriminant' => 'transference',
                'action' => $request->action
            ]
        );

        $origin_account = Account::find($request->origin_account_id);

        $origin_account->historiesOrigin()->save($history);


        $destiny_account = Account::find($request->destiny_account_id);

        $destiny_account->historiesDestiny()->save($history);


        $category = Category::find($request->category_id);

        $category->histories()->save($history);


        $user = Auth::user();

        $user->histories()->save($history);
    }

    static public function createExpense(Request $request)
    {
        $history = new HistoryOfUpdates(
            [
                'description' => $request->description,
                'value' => $request->value,
                'date' => $request->date,
                'status' => $request->status,
                'discriminant' => 'expense',
                'action' => $request->action
            ]
        );

        $account = Account::find($request->account_id);

        $account->historiesOrigin()->save($history);


        $category = Category::find($request->category_id);

        $category->histories()->save($history);


        $user = Auth::user();

        $user->histories()->save($history);

    }

    static public function createIncome(Request $request)
    {
        $history = new HistoryOfUpdates(
            [
                'description' => $request->description,
                'value' => $request->value,
                'date' => $request->date,
                'status' => $request->status,
                'discriminant' => 'history',
                'action' => $request->action
            ]
        );

        $account = Account::find($request->account_id);

        $account->historiesOrigin()->save($history);


        $category = Category::find($request->category_id);

        $category->histories()->save($history);


        $user = Auth::user();

        $user->histories()->save($history);

    }

}
