@extends('layouts.main')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/account/{{$account_id}}">{{\App\Account::find($account_id)->name}}</a>
    </li>

@stop

@section('content')

    <h1>nouveau virement bancaire</h1>

    <form action="/../../../transaction/transference/create" method="post">

        <input type="hidden" name="_token" value="{{csrf_token()}}">

        <input type="hidden" name="origin_account_id" value="{{$account_id}}">

        <div class="form-group">
            <label>description</label>
            <textarea class="form-control" name="description"> {{old('description')}} </textarea>
        </div>

        <div class="form-group">
            <label>value</label>
            <input class="form-control" type="number" step=".01" name="value" value="{{old('value')}}">
        </div>

        <div class="form-group">
            <label>date</label>
            <input class="form-control" type="date" name="date" value="{{old('date')}}">
        </div>

        <div class="form-group">
            <label>category</label>
            <select class="form-control" name="category_id">
                @foreach(\App\Category::all() as $category)
                    <option value="{{$category->id}}" @if(old('category_id') == $category->id) selected @endif>
                        {{$category->name}}
                    </option>
                @endforeach
            </select>

        </div>


        <div class="form-group">
            <label>compte destination</label>
            <select class="form-control" name="destiny_account_id">
            @foreach(\App\Account::where('id', '<>', $account_id)->get() as $account)
                    <option value="{{$account->id}}" @if(old('destiny_account_id') == $account->id) selected @endif>{{$account->name}}</option>
            @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>status</label>
            <div class="form-check">
                <input class="form-check-input" id="paid" type="radio" name="status" value="1" @if(old('status')) checked @endif>
                <label class="form-check-label" for="paid">payé</label>
            </div>

            <div class="form-check">
                <input class="form-check-input" id="unpaid" type="radio" name="status" value="0" @if(!old('status')) checked @endif>
                <label class="form-check-label" for="unpaid">pas payé</label>
            </div>
        </div>

        <button class="btn btn-primary" type="submit"><créer></créer> ajouter virement</button>

    </form>

@stop
