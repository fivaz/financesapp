@extends('layouts.main')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/system/{{$system_id}}"> {{\App\SystemOfAccounts::find($system_id)->name}} </a>
    </li>

@stop

@section('content')

   <form action="/account/create" method="post">

       <input type="hidden" name="_token" value="{{@csrf_token()}}">

       <input type="hidden" name="system_of_accounts_id" value="{{$system_id}}">

       <div class="form-group">
           <label for="name">Nom du Compte</label>
           <input id="name" class="form-control" name="name" value="{{old('name')}}">
       </div>

       <div class="form-group">
           <label for="initial_balance">Balance Initial</label>
           <input id="initial_balance" class="form-control" name="initial_balance" value="{{old('initial_balance')}}">
       </div>

       <button class="btn btn-primary" type="submit">Créer Compte</button>
   </form>

@stop