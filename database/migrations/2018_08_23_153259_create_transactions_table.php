<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('discriminant');
            $table->double('value');
            $table->date('date');
            $table->boolean('status');
            $table->string('description');
            $table->unsignedInteger('origin_account_id');

            $table->unsignedInteger('destiny_account_id');
            $table->timestamps();

            $table->foreign('origin_account_id')->references('id')->on('accounts');
            $table->foreign('destiny_account_id')->references('id')->on('accounts');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
