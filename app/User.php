<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function systemsOfAccounts(){
        return $this->belongsToMany('App\SystemOfAccounts', 'user_system_mapping', 'user_id', 'system_id');
    }

    public function histories(){
        return $this->hasMany('App\HistoryOfUpdates');
    }
}
