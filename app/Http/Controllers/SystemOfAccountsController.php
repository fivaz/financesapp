<?php

namespace App\Http\Controllers;

use App\Http\Requests\SystemRequest;
use App\SystemOfAccounts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SystemOfAccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($system_id)
    {
        $system = SystemOfAccounts::find($system_id);

        $currentBalance = $this->getCurrentBalance($system);

        return view('system.index', ['system' => $system, 'currentBalance' => $currentBalance]);
    }

    public function getCurrentBalance($system)
    {
        $currentBalance = 0;

        foreach($system->accounts as $account)
        {
             $accountController = new AccountController;
             $accountBalance = $accountController->getCurrentBalance($account);
             $currentBalance += $accountBalance;
        }

        return $currentBalance;
    }

    public function new()
    {
        return view('system.new');
    }

    public function create(SystemRequest $request)
    {
        $user = Auth::user();

        $user->systemsOfAccounts()->create(['name' => $request->name]);

        return redirect()->action('HomeController@index');
    }

    public function addCollab($system_id)
    {
        $system = SystemOfAccounts::find($system_id);

        return view('system.add')->withSystem($system);
    }

    public function edit($system_id)
    {
        $system = SystemOfAccounts::find($system_id);

        return view('system.edit')->withSystem($system);
    }

    public function save(SystemRequest $request)
    {
        $system = SystemOfAccounts::find($request->id);

        $system->name = $request->name;

        $system->save();

        return redirect()->action('SystemOfAccountsController@index',['system_id' => $system->id]);
    }

    public function remove($system_id)
    {
        SystemOfAccounts::destroy($system_id);

        return redirect()->action('HomeController@index');
    }
}
