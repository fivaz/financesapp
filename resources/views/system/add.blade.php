@extends('layouts.main')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/system/{{$system->id}}"> {{$system->name}} </a>
    </li>

@stop

@section('content')

    <h1>{{$system->name}}</h1>

    <form action="/system/newMap" method="post">
        <div class="form-group">

            <input type="hidden" name="_token" value="{{csrf_token()}}">

            <input type="hidden" name="system_id" value="{{$system->id}}">

            <label form="email">Entrer l'email de l'utilisateur qui aura accès à <span class="font-weight-bold">{{$system->name}}</span></label>

            <input class="form-control" id="email" type="email" name="email">
        </div>
        <button class="btn btn-primary">donner accès</button>
    </form>

@stop