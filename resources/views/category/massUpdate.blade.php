@extends('layouts.main')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/category/index"> Categories</a>
    </li>

@stop

@section('content')

    <h1>Modifier Status en masse</h1>

    <form action="/category/update" method="post">

        <input type="hidden" name="_token" value="{{@csrf_token()}}">

        <div class="form-group">
            <label>sélectionner le compte</label>
            <select class="form-control" name="account_id">
                @foreach(\App\Account::all() as $account)
                    <option value="{{$account->id}}">{{$account->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>sélectionner la Catégorie</label>
            <select class="form-control" name="category_name">
                @foreach(\App\Category::all() as $category)
                    <option value="{{$category->name}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>sélectionner le nouveau status</label>
            <div class="form-check">
                <input class="form-check-input" id="paid" type="radio" name="status" value="1" checked>
                <label class="form-check-label" for="paid">payé</label>
            </div>

            <div class="form-check">
                <input class="form-check-input" id="unpaid" type="radio" name="status" value="0">
                <label class="form-check-label" for="unpaid">pas payé</label>
            </div>
        </div>

        <button class="btn btn-primary">Modifier status en masse</button>

    </form>

@stop