@extends('layouts.main')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/account/{{$account->id}}">{{$account->name}}</a>
    </li>

@stop

@section('content')

    <h1>Historique d'actions réalisées dans ce compte</h1>

    <table class="table">
        <thead>
            <th class="text-center">utlisateur</th>
            <th class="text-center">action</th>
            <th class="text-center">transaction</th>
            <th class="text-center">description</th>
            <th class="text-center">date</th>
            <th class="text-center">category</th>
            <th class="text-center">destination</th>
            <th class="text-center">status</th>
            <th class="text-center">valeur</th>
        </thead>
       @foreach($account->historiesOrigin as $history)
          <tr>
           <td class="text-center">{{$history->user->name}}</td>
           <td class="text-center">
                @if($history->action == "create") a ajouté
                @else a modifié @endif
           </td>
           <td class="text-center">
                @if($history->discriminant == "income") revenu
                @elseif($history->discriminant == "expense") dépense
                @else virement @endif
           </td>
           <td class="text-center">{{$history->description}}</td>
           <td class="text-center">{{$history->date}}</td>
           <td class="text-center">{{$history->category->name}}</td>
           <td class="text-center">{{$history->destinyAccount->name or ''}}</td>
           <td class="text-center">{{$history->status}}</td>
           <td class="text-center">{{$history->value}} CHF</td>
        </tr>
       @endforeach
   <table>

@stop