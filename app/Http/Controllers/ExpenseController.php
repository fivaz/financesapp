<?php

namespace App\Http\Controllers;

use App\Account;
use App\Category;
use App\Http\Requests\TransactionRequest;
use App\Transaction;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($expense_id)
    {
        $expense = Transaction::find($expense_id);
        return view('transaction.expense.index')->withTransaction($expense);
    }

    public function new($account_id)
    {
        return view('transaction.expense.new')->with('account_id', $account_id);
    }

    public function create(TransactionRequest $request)
    {
        $expense = new Transaction(
            [
                'description' => $request->description,
                'value' => $request->value,
                'date' => $request->date,
                'status' => $request->status,
                'discriminant' => 'expense'
            ]
        );

        $account = Account::find($request->account_id);

        $account->transactions()->save($expense);

        $category = Category::find($request->category_id);

        $category->transactions()->save($expense);


        $request->request->add(['action' => 'create']);

        HistoryController::createExpense($request);

        /*
        return redirect()->action(
            'AccountController@index',
            ['account_id' => $request->account_id]
        );
        */
        return redirect()->action(
            'ExpenseController@new',
            ['account_id' => $request->account_id]
        );
    }

    public function edit($expense_id){
        $expense = Transaction::find($expense_id);
        return view("transaction.expense.edit")->withTransaction($expense);
    }

    public function save(TransactionRequest $request){

        $expense = Transaction::find($request->id);
        $expense->description = $request->description;
        $expense->value = $request->value;
        $expense->date = $request->date;
        $expense->status = $request->status;

        $category = Category::find($request->category_id);

        $category->transactions()->save($expense);

        $expense->save();

        $request->request->add(['account_id' => $expense->originAccount->id]);
        $request->request->add(['action' => 'edit']);

        HistoryController::createExpense($request);

        return redirect()->action('ExpenseController@index', ['transaction_id' => $request->id]);
    }

    public function remove($expense_id)
    {
        $expense = Transaction::find($expense_id);

        $account_id = $expense->originAccount->id;

        $expense->delete();

        return redirect()->action("AccountController@index", ['account_id' => $account_id]);
    }


}
