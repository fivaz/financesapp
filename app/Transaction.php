<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'description',
        'value',
        'date',
        'status',
        'discriminant'
    ];

    public function originAccount(){
        return $this->belongsTo('App\Account', 'origin_account_id', 'id');
    }

    public function destinyAccount()
    {
        return $this->belongsTo('App\Account', 'destiny_account_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id')->withTrashed();
    }

}
