<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSystemMap extends Model
{
    protected $table = "user_system_mapping";
    public $timestamps = true;

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'user_id');
    }

    public function system(){
        return $this->belongsTo('App\System', 'system_id', 'system_id');
    }
}
