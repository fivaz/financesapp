@extends('layouts.main')

@section('navbar-left')

    <li class="nav-item">
        <a class="nav-link" href="/category/massUpdate"> modifications en masse</a>
    </li>

@stop

@section('content')

    <h1>Categories</h1>

    <ul class="list-group">

        @foreach(\App\Category::all() as $category)

            <li class="list-group-item">
                <a href="/category/edit/{{$category->id}}">{{$category->name}}</a>
            </li>

        @endforeach

    </ul>

    <footer>
        <a class="btn btn-primary" href="/category/new">Créer nouvelle Categorie</a>
    <footer>

@stop