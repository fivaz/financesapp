@extends('layouts.main')


@section('navbar-right')
    
    <li class="nav-item">
        <a class="nav-link" href="/system/{{$account->systemOfAccounts->id}}">{{$account->systemOfAccounts->name}}</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="/account/indexQB/{{$account->id}}">QB</a>
    </li>

@stop

@section('navbar-left')

    <li class="nav-item">
        <a class="nav-link" href="/history/{{$account->id}}">historique de modifications</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="/account/edit/{{$account->id}}">modifier compte</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" data-toggle="modal" data-target="#is-sure" href="#">effacer compte</a>
    </li>

    <div class="modal fade" id="is-sure" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Vous voulez effacer ce compte?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuller</button>
            <a type="button" class="btn btn-primary" href="/account/remove/{{$account->id}}">Effacer</a>
          </div>
        </div>
      </div>
    </div>

@stop

@section('content')

    <div class="row">
        <h1 class="col">{{$account->name}}</h1>

        <h1 class="col text-right">solde: {{$currentBalance}} CHF</h1>
    </div>

    <table class="table table-hover">
        <thead>
            <th class="text-center">transaction</th>
            <th class="text-center">description</th>
            <th class="text-center">category</th>
            <th class="text-center">date</th>
            <th class="text-center">status</th>
            <th class="text-center">valeur</th>
        </thead>

        @foreach($account->allTransactions as $transaction)
            <tr class="pointer" onclick="window.location='../../transaction/{{$transaction->discriminant}}/index/{{$transaction->id}}';">
                <td class="text-center">
                    @if($transaction->discriminant == "income") revenu
                    @elseif($transaction->discriminant == "transference") virement
                    @else($transaction->discriminant == "expense") dépense @endif
                </td>
                <td class="text-center">{{$transaction->description}}</td>
                <td class="text-center">{{$transaction->category->name}}</td>
                <td class="text-center">{{$transaction->date}}</td>
                <td class="text-center">{{$transaction->status ? "payé" : "pas payé"}}</td>
                <td class="text-center">{{$transaction->value}} CHF</td>
            </tr>
        @endforeach
    </table>

    <footer>
        <div class="d-flex justify-content-around">
            <a class="btn btn-secondary" href="/transaction/transference/new/{{$account->id}}">ajouter virement</a>
            <a class="btn btn-danger" href="/transaction/expense/new/{{$account->id}}">ajouter dépense</a>
            <a class="btn btn-success" href="/transaction/income/new/{{$account->id}}">ajouter revenu</a>
        </div>
    </footer>

@stop