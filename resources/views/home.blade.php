@extends('layouts.main')

@section('navbar-left')

    <li class="nav-item">
        <a class="nav-link" href="/category"> categories </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" data-toggle="modal" data-target="#is-sure" href="#">effacer vos systèmes de comptes</a>
    </li>

    <div class="modal fade" id="is-sure" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Vous voulez effacer tous les systemes de comptes?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuller</button>
                    <a type="button" class="btn btn-primary" href="/removeAll">Effacer</a>
                </div>
            </div>
        </div>
    </div>

@stop


@section('content')

        <h1>Systèmes de comptes</h1>

        @if(empty($systems->where('isDeleted', 0)))

            <p class="alert alert-info">Vous n'avez pas encore un système de compte, créez-le</p>

        @else

            <ul class="list-group">

                @foreach($systems->where('isDeleted', 0) as $system)
                    <li class="list-group-item">
                        <a href="/system/{{$system->id}}">{{$system->name}}</a>
                    </li>
                @endforeach

            </ul>

        @endif

        <footer>
            <a class="btn btn-primary" href="system/new">Créer nouveau Système de comptes</a>
        </footer>
@stop

