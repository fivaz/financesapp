<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $fillable = ['name'];

    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function transactions(){
       return $this->hasMany('App\Transaction');
    }

    public function histories(){
        return $this->hasMany('App\HistoryOfUpdates');
    }
}
