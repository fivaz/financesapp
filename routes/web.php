<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');


Route::get('/system/new', 'SystemOfAccountsController@new');

Route::get('/system/create', 'SystemOfAccountsController@create');

Route::get('/system/{system_id}', 'SystemOfAccountsController@index');

Route::get('/system/add/{system_id}', 'SystemOfAccountsController@addCollab');

Route::get('/system/edit/{system_id}', 'SystemOfAccountsController@edit');

Route::post('/system/save', 'SystemOfAccountsController@save');

Route::get('/system/remove/{system_id}', 'SystemOfAccountsController@remove');


Route::post('/system/newMap', 'UserSystemMapController@newMap');


Route::get('/removeAll', 'UserSystemMapController@removeAll');


Route::get('/account/new/{system_id}', 'AccountController@new');

Route::post('/account/create', 'AccountController@create');

Route::get('/account/{account_id}', 'AccountController@index');

//Route::get('/account/getQB/{account_id}', 'AccountController@indexQB');

Route::get('/account/indexQB/{account_id}', 'AccountController@indexQB');

Route::get('/account/edit/{account_id}', 'AccountController@edit');

Route::post('/account/save', 'AccountController@save');

Route::get('/account/remove/{account_id}', 'AccountController@remove');


Route::get('/transaction/expense/new/{account_id}', 'ExpenseController@new');

Route::post('/transaction/expense/create', 'ExpenseController@create');

Route::get('/transaction/expense/index/{expense_id}', 'ExpenseController@index');

Route::get('/transaction/expense/edit/{expense_id}', 'ExpenseController@edit');

Route::post('/transaction/expense/save', 'ExpenseController@save');

Route::get('/transaction/expense/remove/{expense_id}', 'ExpenseController@remove');



Route::get('/transaction/income/new/{account_id}', 'IncomeController@new');

Route::post('/transaction/income/create', 'IncomeController@create');

Route::get('/transaction/income/index/{expense_id}', 'IncomeController@index');

Route::get('/transaction/income/edit/{expense_id}', 'IncomeController@edit');

Route::post('/transaction/income/save', 'IncomeController@save');

Route::get('/transaction/income/remove/{expense_id}', 'IncomeController@remove');



Route::get('/transaction/transference/new/{account_id}', 'TransferenceController@new');

Route::post('/transaction/transference/create', 'TransferenceController@create');

Route::get('/transaction/transference/index/{transference_id}', 'TransferenceController@index');

Route::get('/transaction/transference/edit/{transference_id}', 'TransferenceController@edit');

Route::post('/transaction/transference/save', 'TransferenceController@save');

Route::get('/transaction/transference/remove/{expense_id}', 'TransferenceController@remove');


Route::get('/history/{account_id}', 'HistoryController@index');


Route::get('/category', 'CategoryController@index');

Route::get('/category/new', 'CategoryController@new');

Route::get('/category/create', 'CategoryController@create');

Route::get('/category/edit/{id}', 'CategoryController@edit');

Route::get('/category/save', 'CategoryController@save');

Route::get('/category/remove/{id}', 'CategoryController@remove');

Route::get('/category/massUpdate', 'CategoryController@massUpdate');

Route::post('/category/update', 'CategoryController@update');

