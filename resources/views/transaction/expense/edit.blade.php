@extends('layouts.main')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/account/{{$transaction->originAccount->id}}">{{$transaction->originAccount->name}}</a>
    </li>

@stop

@section('content')

    <h1>Dépense</h1>

    <form action="/../../../transaction/expense/save" method="post">

        <input type="hidden" name="_token" value="{{csrf_token()}}">

        <input type="hidden" name="id" value="{{$transaction->id}}">

        <div class="form-group">
            <label>description</label>
            <textarea class="form-control" name="description">{{$transaction->description}}</textarea>
        </div>

        <div class="form-group">
            <label>value</label>
            <input class="form-control" type="number" step=".01" name="value" value="{{$transaction->value}}">
        </div>

        <div class="form-group">
            <label>date</label>
            <input class="form-control" type="date" name="date" value="{{$transaction->date}}">
        </div>

        <div class="form-group">
            <label>category</label>
            <select class="form-control" name="category_id">
                @foreach(\App\Category::all() as $category)
                    <option value="{{$category->id}}" @if($transaction->category->id == $category->id) selected @endif>{{$category->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>status</label>
            <div class="form-check">
                <input class="form-check-input" id="paid" type="radio" name="status" value="1" {{$transaction->status ? "checked" : ""}}>
                <label class="form-check-label" for="paid">payé</label>
            </div>

            <div class="form-check">
                <input class="form-check-input" id="unpaid" type="radio" name="status" value="0" {{!$transaction->status ? "checked" : ""}}>
                <label class="form-check-label" for="unpaid">pas payé</label>
            </div>
        </div>

        <button class="btn btn-primary" type="submit">modifier dépense</button>

@stop