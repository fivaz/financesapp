@extends('layouts.system')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/category"> Catégories </a>
    </li>

@stop

@section('navbar-left')

    <li class="nav-item">
        <a class="nav-link" data-toggle="modal" data-target="#is-sure" href="#">Effacer catégorie</a>
    </li>

    <div class="modal fade" id="is-sure" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Vous voulez effacer cette catégorie?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuller</button>
                    <a type="button" class="btn btn-primary" href="/category/remove/{{$category->id}}">Effacer</a>
                </div>
            </div>
        </div>
    </div>

@stop

@section('content')

    <form action="/category/save" action="post">

        <div class="form-group">

            <input type="hidden" name="id" value="{{$category->id}}">

            <label for="name">Nom de la Catégorie</label>
            <input id="name" class="form-control" name="name" value="{{$category->name}}"/>
        </div>

        <button class="btn btn-primary">modifier Catégorie</button>
    </form>

@stop


