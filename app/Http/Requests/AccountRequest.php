<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'initial_balance' => 'required|numeric|max:9999999999999999'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Le nom est obligatoire',
            'initial_balance.required' => 'Le solde est obligatoire',
            'numeric' => 'Le solde doit un être nombre',
            'max' => 'Le solde est trop grande',
        ];
    }
}
