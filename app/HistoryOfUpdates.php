<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryOfUpdates extends Model
{
    protected $table = 'histories_of_updates';

    protected $fillable = [
        'description',
        'value',
        'date',
        'status',
        'discriminant',
        'action'
    ];

    public function originAccount()
    {
        return $this->belongsTo('App\Account', 'origin_account_id', 'id');
    }

    public function destinyAccount()
    {
        return $this->belongsTo('App\Account', 'destiny_account_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

}
