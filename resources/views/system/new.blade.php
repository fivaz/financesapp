@extends('layouts.system')

@section('content')

    <form action="{{action('SystemOfAccountsController@create')}}" action="post">

        <div class="form-group">
            <label for="name">Nom du Système</label>
            <input id="name" class="form-control" name="name" value="{{old('name')}}"/>
        </div>

        <button class="btn btn-primary">Créer Système</button>
    </form>

@stop


