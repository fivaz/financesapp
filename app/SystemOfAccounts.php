<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemOfAccounts extends Model
{
    protected $table = "systems_of_accounts";

    protected $fillable = ['name'];

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function accounts(){
        return $this->hasMany('App\Account');
    }
}
