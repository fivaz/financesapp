<?php

use Illuminate\Database\Seeder;
use \stock\Category;
use \stock\Product;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CategoriesTableSeeder::class);
    }
}

class CategoriesTableSeeder extends Seeder{

    public function run()
    {
        Category::create(['name' => 'Homme appliance']);
        Category::create(['name' => 'Consumer electronics']);
        Category::create(['name' => 'Toys']);
        Category::create(['name' => 'Clothing']);
    }
}

class ProductsTableSeeder extends Seeder{

    public function run()
    {
        Product::create([
            'nome' => 'Stoven',
            'descricao' => 'a black Stoven',
            'valor' => '500',
            'quantidade' => '5',
            'tamanho' => '100cm x 130cm',
            'category_id' => '1',
        ]);
    }
}