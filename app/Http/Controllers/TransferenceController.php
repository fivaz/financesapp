<?php

namespace App\Http\Controllers;

use App\Account;
use App\Category;
use App\Http\Requests\TransactionRequest;
use App\Transaction;
use Illuminate\Http\Request;

class TransferenceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($transference_id)
    {
        $transference = Transaction::find($transference_id);
        return view('transaction.transference.index')->withTransaction($transference);
    }

    public function new($account_id)
    {
        return view('transaction.transference.new')->with('account_id', $account_id);
    }

    public function create(TransactionRequest $request)
    {
        $transference = new Transaction(
            [
                'description' => $request->description,
                'value' => $request->value,
                'date' => $request->date,
                'status' => $request->status,
                'discriminant' => 'transference'
            ]
        );

        $origin_account = Account::find($request->origin_account_id);

        $origin_account->transactions()->save($transference);


        $destiny_account = Account::find($request->destiny_account_id);

        $destiny_account->destinationOfTransactions()->save($transference);


        $category = Category::find($request->category_id);

        $category->transactions()->save($transference);


        $request->request->add(['action' => 'create']);

        HistoryController::createTransference($request);

        return redirect()->action('AccountController@index', ['account_id' => $request->origin_account_id]);
    }

    public function edit($transference_id){
        $transference = Transaction::find($transference_id);
        return view("transaction.transference.edit")->withTransaction($transference);
    }

    public function save(TransactionRequest $request)
    {
        $transference = Transaction::find($request->id);
        $transference->description = $request->description;
        $transference->value = $request->value;
        $transference->date = $request->date;
        $transference->status = $request->status;

        $category = Category::find($request->category_id);

        $category->transactions()->save($transference);

        $destiny_account = Account::find($request->destiny_account_id);

        $destiny_account->destinationOfTransactions()->save($transference);

        $transference->save();

        $request->request->add(['origin_account_id' => $transference->originAccount->id]);
        $request->request->add(['destiny_account_id' => $transference->destinyAccount->id]);
        $request->request->add(['action' => 'edit']);

        HistoryController::createTransference($request);

        return redirect()->action('TransferenceController@index', ['transference_id' => $request->id]);
    }

    public function remove($transference_id)
    {
        $transference = Transaction::find($transference_id);

        $account_id = $transference->originAccount->id;

        $transference->delete();

        return redirect()->action("AccountController@index", ['account_id' => $account_id]);
    }

}
