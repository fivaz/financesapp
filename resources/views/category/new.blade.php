@extends('layouts.system')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/category"> Categories </a>
    </li>

@stop

@section('content')

    <form action="/category/create" action="post">

        <div class="form-group">

            <label for="name">Nom de la Catégorie</label>
            <input id="name" class="form-control" name="name"/>
        </div>

        <button class="btn btn-primary">Créer Catégorie</button>
    </form>

@stop


