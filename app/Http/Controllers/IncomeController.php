<?php

namespace App\Http\Controllers;

use App\Account;
use App\Category;
use App\Http\Requests\TransactionRequest;
use App\Transaction;
use Illuminate\Http\Request;

class IncomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($income_id)
    {
        $income = Transaction::find($income_id);
        return view('transaction.income.index')->withTransaction($income);
    }

    public function new($account_id)
    {
        return view('transaction.income.new')->with('account_id', $account_id);
    }

    public function create(TransactionRequest $request)
    {
        $income = new Transaction(
            [
                'description' => $request->description,
                'value' => $request->value,
                'date' => $request->date,
                'status' => $request->status,
                'discriminant' => 'income'
            ]
        );

        $account = Account::find($request->account_id);

        $account->transactions()->save($income);

        $category = Category::find($request->category_id);

        $category->transactions()->save($income);

        $request->request->add(['action' => 'create']);

        HistoryController::createIncome($request);
        return redirect()->action(
            'AccountController@index',
            ['account_id' => $request->account_id]
        );

    }

    public function edit($income_id){
        $income = Transaction::find($income_id);
        return view("transaction.income.edit")->withTransaction($income);
    }

    public function save(TransactionRequest $request){

        $income = Transaction::find($request->id);
        $income->description = $request->description;
        $income->value = $request->value;
        $income->date = $request->date;
        $income->status = $request->status;

        $category = Category::find($request->category_id);

        $category->transactions()->save($income);

        $income->save();

        $request->request->add(['account_id' => $income->originAccount->id]);
        $request->request->add(['action' => 'edit']);

        HistoryController::createIncome($request);

        return redirect()->action('IncomeController@index', ['transaction_id' => $request->id]);
    }

    public function remove($income_id)
    {
        $income = Transaction::find($income_id);

        $account_id = $income->originAccount->id;

        $income->delete();

        return redirect()->action("AccountController@index", ['account_id' => $account_id]);
    }

}
