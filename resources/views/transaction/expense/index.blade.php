@extends('layouts.main')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/account/{{$transaction->originAccount->id}}"> {{$transaction->originAccount->name}} </a>
    </li>

@stop

@section('navbar-left')

    <li class="nav-item">
        <a class="nav-link" href="/transaction/expense/edit/{{$transaction->id}}">modifier dépense</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" data-toggle="modal" data-target="#is-sure" href="#">effacer dépense</a>
    </li>

    <div class="modal fade" id="is-sure" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Vous voulez effacer cette transaction?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuller</button>
            <a type="button" class="btn btn-primary" href="/transaction/expense/remove/{{$transaction->id}}">Effacer</a>
          </div>
        </div>
      </div>
    </div>

@stop

@section('content')

        <h1>Dépense</h1>

        <dl class="row">
          <dt class="col-sm-2">Description: </dt>
          <dd class="col-sm-10">{{$transaction->description}}</dd>
          <dt class="col-sm-2">Valeur: </dt>
          <dd class="col-sm-10">{{$transaction->value}}</dd>
          <dt class="col-sm-2">Category: </dt>
          <dd class="col-sm-10">{{$transaction->category->name}}</dd>
          <dt class="col-sm-2">Date: </dt>
          <dd class="col-sm-10">{{$transaction->date}}</dd>
          <dt class="col-sm-2">Status: </dt>
          <dd class="col-sm-10">{{$transaction->status ? "payé" : "pas payé"}}</dd>

        </dl>

@stop