@extends('layouts.system')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/system/{{$system->id}}"> {{$system->name}} </a>
    </li>

@stop

@section('content')

    <form action="/system/save" method="post">

        <input type="hidden" name="_token" value="{{@csrf_token()}}">

        <input type="hidden" name="id" value="{{$system->id}}">

        <div class="form-group">
            <label for="name">Nom du Système</label>
            <input id="name" class="form-control" name="name" value="{{$system->name}}"/>
        </div>

        <button class="btn btn-primary">Créer Système</button>
    </form>

@stop


