<?php

namespace App\Http\Controllers;

use App\Account;
use App\Http\Requests\AccountRequest;
use App\SystemOfAccounts;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($account_id)
    {
        $account = Account::find($account_id);

        $currentBalance = $this->getCurrentBalance($account);

        return view('account.index', ['account' => $account, 'currentBalance' => $currentBalance]);
    }

    public function getCurrentBalance($account)
    {
        $currentBalance = $account->initial_balance;

        foreach ($account->allTransactions as $transaction) {
            if ($transaction->discriminant == "income") {
                $currentBalance += $transaction->value;
            } else {
                if ($transaction->discriminant == "transference" && $transaction->destiny_account_id == $account->id) {
                    $currentBalance += $transaction->value;
                }else{
                    $currentBalance -= $transaction->value;
                }
            }
        }

        return $currentBalance;
    }

    public function indexQB($account_id)
    {
        $account = DB::select("
            SELECT 
            (SUM(CASE WHEN transactions.discriminant = 'income' 
            THEN transactions.value ELSE transactions.value*-1 END) + accounts.initial_balance) AS balance , accounts.name, accounts.id 
            FROM accounts 
            INNER JOIN transactions 
            ON transactions.origin_account_id = accounts.id
            WHERE transactions.origin_account_id = :account_id 
            GROUP BY accounts.name, accounts.id, accounts.initial_balance;
            ", ['account_id' => $account_id]
        );

        $transactions = DB::select("
            SELECT
            transactions.id, transactions.description, transactions.value, 
            transactions.discriminant, transactions.date, transactions.status, categories.name as category_name
            FROM transactions 
            INNER JOIN categories
            ON categories.id = transactions.category_id 
            WHERE transactions.origin_account_id = :account_id ", ['account_id' => $account_id]
        );

        return view('account.indexQB', ['account' => $account[0], 'transactions' => $transactions]);
    }

    public function new($system_id)
    {
        return view('account.new')->with('system_id', $system_id);
    }

    public function create(AccountRequest $request)
    {
        $system = SystemOfAccounts::find($request->system_of_accounts_id);

        $system->accounts()->create([
            'name' => $request->name,
            'initial_balance' => $request->initial_balance
        ]);

        return redirect()->action('SystemOfAccountsController@index', ['system_id' => $request->system_of_accounts_id]);
    }

    public function edit($account_id)
    {
        $account = Account::find($account_id);
        return view('account.edit')->withAccount($account);
    }

    public function save(AccountRequest $request)
    {
        $account = Account::find($request->id);
        $account->name = $request->name;
        $account ->initial_balance = $request->initial_balance;

        $account ->save();

        return redirect()->action('AccountController@index', ['account_id' => $account->id]);
    }

    public function remove($account_id)
    {
        $account = Account::find($account_id);
        $system_id = $account->systemOfAccounts->id;

        $account->delete();

        return redirect()->action('SystemOfAccountsController@index', ['system_id' => $system_id]);
    }
}
