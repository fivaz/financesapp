<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required|min:3',
            'value' => 'required|numeric|max:9999999999999999',
            'date' => 'required|date',
            'category_id' => 'required|numeric',
            'status' => 'required|boolean'
        ];
    }

    public function messages()
    {
        return [
            'status.required' => 'Le status est obligatoire',
            'required' => 'La :attribute est obligatoire',
            'numeric' => 'La :attribute doit un être nombre',
            'boolean' => 'Le status est invalide',
            'min' => 'La description doit avoir au minimum 3 charactères',
            'max' => 'La valeur est trop grande',
            'date' => 'date invalide',
        ];
    }
}
