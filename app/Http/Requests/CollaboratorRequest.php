<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class CollaboratorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'email' => 'exists:users,email'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => "L'email est obligatoire",
            'email.exists' => "L'email n'appartient pas à un de nos utilisateurs"
        ];
    }

}
