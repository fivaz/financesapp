@extends('layouts.main')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/system/{{$system->id}}"> {{$system->name}} </a>
    </li>

@stop

@section('navbar-left')

    <li class="nav-item">
        <a class="nav-link" href="/system/edit/{{$system->id}}"> modifier système </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" data-toggle="modal" data-target="#is-sure" href="#">effacer système</a>
    </li>

    <div class="modal fade" id="is-sure" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Vous voulez effacer ce compte?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuller</button>
            <a type="button" class="btn btn-primary" href="/system/remove/{{$system->id}}">Effacer</a>
          </div>
        </div>
      </div>
    </div>

    <li class="nav-item">
        <a class="nav-link" href="/system/add/{{$system->id}}"> ajouter collaborateur </a>
    </li>

@stop

@section('content')

    <div class="row">
        <h1 class="col">{{$system->name}}</h1>
        <h1 class="col text-right">{{$currentBalance}}</h1>
    </div>

    <ul class="list-group">

        @foreach($system->accounts as $account)

        <li class="list-group-item">
            <div class="row">
                <a class="col" href="/account/{{$account->id}}">{{$account->name}}</a>

                <?php $accountController = new App\Http\Controllers\AccountController; ?>

                <a class="col text-right" href="/account/{{$account->id}}">{{$accountController->getCurrentBalance($account)}}</a>
            </div>
        </li>

        @endforeach

    </ul>

    <footer>
        <a class="btn btn-primary" href="../account/new/{{$system->id}}">Créer nouveau Compte</a>
    <footer>

@stop