<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriesOfUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories_of_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->double('value');
            $table->boolean('status');
            $table->date('date');
            $table->string('discriminant');
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('origin_account_id');
            $table->unsignedInteger('destiny_account_id')->nullable();
            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')
                ->on('categories');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->foreign('origin_account_id')
                ->references('id')
                ->on('accounts');
            $table->foreign('destiny_account_id')
                ->references('id')
                ->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories_of_updates');
    }
}
