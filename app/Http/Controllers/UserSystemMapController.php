<?php

namespace App\Http\Controllers;

use App\Http\Requests\CollaboratorRequest;
use App\SystemOfAccounts;
use App\User;
use App\UserSystemMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserSystemMapController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function newMap(CollaboratorRequest $request){

        $user = User::where('email', $request->email)->first();

        $user->systemsOfAccounts()->attach($request->system_id);

        return redirect()->action('SystemOfAccountsController@index', ['system_id' => $request->system_id]);
    }

    public function removeAll(){

        DB::delete("UPDATE systems_of_accounts 
          INNER JOIN user_system_mapping 
          ON user_system_mapping.system_id = systems_of_accounts.id 
          SET systems_of_accounts.isDeleted = 1
          WHERE user_id = :user_id", ['user_id' => Auth::user()->id]);

        return redirect()->action('HomeController@index');
    }
}
