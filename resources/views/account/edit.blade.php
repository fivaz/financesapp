@extends('layouts.main')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/account/{{$account->id}}"> {{$account->name}} </a>
    </li>

@stop

@section('content')

    <div>
        <form action="/account/save" method="post">

            <input type="hidden" name="_token" value="{{@csrf_token()}}">

            <input type="hidden" name="id" value="{{$account->id}}">

            <div class="form-group">
                <label for="name">Nom du Compte</label>
                <input id="name" class="form-control" name="name" value="{{$account->name}}">
            </div>

            <div class="form-group">
                <label for="initial_balance">Solde Initial</label>
                <input id="initial_balance" class="form-control" name="initial_balance" value="{{$account->initial_balance}}">
            </div>

            <button class="btn btn-primary" type="submit">modifier Compte</button>
        </form>
    </div>

@stop