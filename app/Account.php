<?php

namespace App;

use App\Http\Controllers\AccountController;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'initial_balance', 'system_of_accounts_id'];

    protected $dates = ['deleted_at'];

    public function systemOfAccounts(){
        return $this->belongsTo('App\SystemOfAccounts');
    }

    public function transactions(){
        return $this->hasMany('App\Transaction', 'origin_account_id', 'id');
    }

    public function destinationOfTransactions(){
        return $this->hasMany('App\Transaction', 'destiny_account_id', 'id');
    }

    public function allTransactions(){
        return $this->transactions()->union($this->destinationOfTransactions()->toBase())->orderBy('date');
    }

    public function historiesOrigin(){
        return $this->hasMany('App\HistoryOfUpdates', 'origin_account_id', 'id');
    }

    public function historiesDestiny(){
        return $this->hasMany('App\HistoryOfUpdates', 'destiny_account_id', 'id');
    }
}
