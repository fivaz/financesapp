@extends('layouts.main')

@section('navbar-right')

    <li class="nav-item">
        <a class="nav-link" href="/account/{{$account->id}}">model</a>
    </li>

@stop

@section('navbar-left')

    <li class="nav-item">
        <a class="nav-link" href="/history/{{$account->id}}">historique de modifications</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="/account/edit/{{$account->id}}">edit compte</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="/account/remove/{{$account->id}}">effacer compte</a>
    </li>

@stop

@section('content')

    <div class="row">
        <h1 class="col">{{$account->name}}</h1>

        <h1 class="col text-right">solde: {{$account->balance}} CHF</h1>
    </div>

    <table class="table table-hover">
        <thead>
            <th class="text-center">transaction</th>
            <th class="text-center">description</th>
            <th class="text-center">category</th>
            <th class="text-center">date</th>
            <th class="text-center">status</th>
            <th class="text-center">valeur</th>
        </thead>

        @foreach($transactions as $transaction)
            <tr class="pointer" onclick="window.location='../../transaction/{{$transaction->discriminant}}/index/{{$transaction->id}}';">
                <td  class="text-center">
                    @if($transaction->discriminant == "income") revenu
                    @elseif($transaction->discriminant == "transference") virement
                    @else($transaction->discriminant == "expense") dépense @endif
                </td>
                <td class="text-center">{{$transaction->description}}</td>
                <td class="text-center">{{$transaction->category_name}}</td>
                <td class="text-center">{{$transaction->date}}</td>
                <td class="text-center">{{$transaction->status ? "payé" : "pas payé"}}</td>
                <td class="text-center">{{$transaction->value}} CHF</td>
            </tr>
        @endforeach
    </table>

    <footer>
        <div class="d-flex justify-content-around">
            <a class="btn btn-secondary" href="/transaction/transference/new/{{$account->id}}">ajouter virement</a>
            <a class="btn btn-danger" href="/transaction/expense/new/{{$account->id}}">ajouter dépense</a>
            <a class="btn btn-success" href="/transaction/income/new/{{$account->id}}">ajouter revenu</a>
        </div>
    </footer>

@stop