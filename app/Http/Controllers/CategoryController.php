<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('category.index');
    }

    public function new()
    {
        return view('category.new');
    }

    public function create(Request $request)
    {
        Category::create(['name' => $request->name]);

        return view('category.index');
    }

    public function edit(Request $request)
    {
        $category = Category::find($request->id);

        return view('category.edit')->withCategory($category);
    }

    public function save(Request $request)
    {
        $category = Category::find($request->id);
        $category->name = $request->name;

        $category->save();

        return redirect()->action('CategoryController@index');
    }

    public function remove($id)
    {
        Category::find($id)->delete();

        return redirect()->action('CategoryController@index');
    }

    public function massUpdate()
    {
        return view('category.massUpdate');
    }

    public function update(Request $request)
    {
        DB::update("
            UPDATE transactions 
            INNER JOIN categories 
            ON categories.id = transactions.category_id 
            SET transactions.status = :status
            WHERE categories.name = :category_name 
            AND transactions.origin_account_id = :account_id
        ",  [
                'category_name' => $request->category_name,
                'account_id' => $request->account_id,
                'status' => $request->status
            ]
        );

        return redirect()->action('AccountController@index', ['account_id' => $request->account_id]);
    }
}
